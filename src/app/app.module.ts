import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MorpionComponent } from './Page/morpion/morpion.component';

import { GridComponent } from './Component/grid/grid.component';
import { ScoreBoardComponent } from './Component/score-board/score-board.component';
import { VictoryComponent } from './Component/victory/victory.component';
import { PlayerCardComponent } from './Component/player-card/player-card.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    MorpionComponent,
    AppComponent,
    GridComponent,
    ScoreBoardComponent,
    VictoryComponent,
    PlayerCardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
