import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-score-board',
  templateUrl: './score-board.component.html',
  styleUrls: ['./score-board.component.scss']
})
export class ScoreBoardComponent implements OnInit, OnChanges {
  
  crossScore: number = 0;
  circleScore: number = 0;
  player_1 = "Player 1";
  player_2 = "Player 2";

  @Input() score: 'crossWin' | 'circleWin'
  @Input() players: object
  constructor() { }

  ngOnInit(): void {    
  }
  
  ngOnChanges(): void{    
    if (this.score === 'crossWin') {
      this.crossScore = this.crossScore + 1;
    }else if(this.score === 'circleWin') {
      this.circleScore = this.circleScore + 1;
    }

    if(this.players) {
      if(this.players['player1']['name'] !=''){
        this.player_1 = this.players["player1"]['name'];
      }
      if(this.players['player2']['name'] !='') {
        this.player_2 = this.players["player2"]['name'];
      }
      document.documentElement.style.setProperty('--colorPlayer_1', this.players['player1']['color'])
      document.documentElement.style.setProperty('--colorPlayer_2', this.players['player2']['color']) 
    }  


  }



}
