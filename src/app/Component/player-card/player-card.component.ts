import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-player-card',
  templateUrl: './player-card.component.html',
  styleUrls: ['./player-card.component.scss']
})
export class PlayerCardComponent implements OnInit {
  showVictoryCard: boolean;
  @Output() Players =  new EventEmitter<object>();
  constructor() { }

  ngOnInit(): void {
  }

  play() {
    let namePlayer_1 = (<HTMLInputElement>document.getElementById("player_1")).value
    let namePlayer_2 = (<HTMLInputElement>document.getElementById("player_2")).value

    let colorPlayer_1 = (<HTMLInputElement>document.getElementById("colorPlayer_1")).value
    let colorPlayer_2 = (<HTMLInputElement>document.getElementById("colorPlayer_2")).value

   let players = {
     'player1': {
       'name' : namePlayer_1,
       'color': colorPlayer_1
     },
     'player2': {
      'name' : namePlayer_2,
      'color': colorPlayer_2
    }
   }
   
    this.Players.emit(players)
  
    
  }
}
