import { Component, Input, OnInit, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-victory',
  templateUrl: './victory.component.html',
  styleUrls: ['./victory.component.scss']
})
export class VictoryComponent implements OnInit, OnChanges {
  @Input() score: 'crossWin' | 'circleWin'
  @Input() showVictoryCardEmit: boolean
  @Input() players: object
  @Output() reset: EventEmitter<string> = new EventEmitter();
  showVictoryCard = false;
  winner = ""
  player_1 = "Player 1";
  player_2 = "Player 2";

  constructor() { }
  ngOnInit(): void {
  this.showVictoryCard = false;

  }
  
  ngOnChanges(): void {    
    if(this.score === "crossWin"){
      this.winner = this.player_1+" Wins"
      this.showVictoryCard = true  
    } else if(this.score === "circleWin"){
      this.winner = this.player_2+" Wins"
      this.showVictoryCard = true  
    } else if(this.score === "equality"){
      this.winner = "Nobody wins"
      this.showVictoryCard = true  
    } 

    if(this.players) {
      if(this.players['player1']['name'] !=''){
        this.player_1 = this.players["player1"]['name'];
      }
      if(this.players['player2']['name'] !='') {
        this.player_2 = this.players["player2"]['name'];
      }
    }  

  }

  resetGrid() {
    this.reset.emit('reset')
    this.showVictoryCard = false;
  }
}

