
import { stringify } from '@angular/compiler/src/util';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {
  
  cases: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  victory: string ;
  showPlayerCard: boolean;
  crossPlayer = "X";
  RondPlayer = "O";
  CurrentPlayer = this.crossPlayer;
  players: object;
  colorPlayer_1 = '#ff2d95';
  colorPlayer_2= '#3548f7'



  possibility = {
    arr1: ['1', '2', '3'],
    arr2: ['4', '5', '6'],
    arr3: ['7', '8', '9'],
    arr4: ['1', '4', '7'],
    arr5: ['2', '5', '8'],
    arr6: ['3', '6', '9'],
    arr7: ['1', '5', '9'],
    arr8: ['3', '5', '7'],
  }

  constructor() { }

  ngOnInit(): void {  
    this.showPlayerCard = true; 
    document.documentElement.style.setProperty('--colorPlayer_1', this.colorPlayer_1)
    document.documentElement.style.setProperty('--colorPlayer_2', this.colorPlayer_2) 
  }
  
  setPlayers(names: object){
    this.players = names
    this.colorPlayer_1 = names['player1']['color']
    this.colorPlayer_2 = names['player2']['color']
    this.showPlayerCard = false;

    document.documentElement.style.setProperty('--colorPlayer_1', this.colorPlayer_1)
    document.documentElement.style.setProperty('--colorPlayer_2', this.colorPlayer_2) 
 }

  putShape(index: string){
    let zone = document.getElementById(index)

    if (zone.innerText == '') {
      zone.innerText = this.CurrentPlayer;
      if(this.CurrentPlayer === "X") {
        zone.classList.add('cross')
      } else if(this.CurrentPlayer === "O") {
        zone.classList.add('circle')
      }
      if(this.CurrentPlayer != this.RondPlayer){
        this.CurrentPlayer = this.RondPlayer
      }else {
        this.CurrentPlayer = this.crossPlayer
      }
    }

    this.determineHowWin()
  }

  determineHowWin() {
    for (const key in this.possibility) {
      //console.log(this.possibility[key]);
      for (const caseNumber of this.possibility[key]) {
        //console.log(caseNumber + " = "+ document.getElementById(caseNumber).textContent);
        if (this.possibility[key].every((caseNumbers: string) => document.getElementById(caseNumbers).textContent === 'X')) {
          this.victory = "crossWin"
        } else if(this.possibility[key].every((caseNumbers: string) => document.getElementById(caseNumbers).textContent === 'O')) {
          this.victory = "circleWin"
        } 
      }
    }


    let caseData = []
    for(const button of this.cases){
      let buttonStringify = stringify(button)
      let element = document.getElementById(buttonStringify).innerText
      if(element === 'X' || element === 'O') {
        caseData.push(element)
      }
    }
    if (caseData.length === 9 && this.victory !== 'crossWin' && this.victory !== 'circleWin') {
      this.victory = 'equality'
    }
  } 

  resetGrid() {
    for (const index of this.cases) {
      const indexStringify = index.toString()
      let zone = document.getElementById(indexStringify)
      zone.innerText = ''
      this.victory = null
      zone.classList.remove('cross')
      zone.classList.remove('circle')
    }    
  }
  



}
