import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MorpionComponent } from './Page/morpion/morpion.component';

const routes: Routes = [
 // {path: 'nomdupath', component: Lecomponent}
  {path: 'morpion', component: MorpionComponent},
  {path: '', redirectTo: '/morpion', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
